from setuptools import find_packages, setup
import hangman

setup(
    name="hangman",
    packages=find_packages(
        include=["hangman"],
    ),
    include_package_data=True,
    version=hangman.__version__,
    description="Game for fun",
    author="Nikita Balagansky",
)