from dataclasses import dataclass, field
from typing import Tuple

from hangman.state import State
from hangman.hangman_pics import HANGMAN_PICS
import random


@dataclass
class Game:

    words: Tuple[str]
    num_lives: int = 6
    state: State = field(init=False)

    def __post_init__(self):
        word_to_guess = random.choice(self.words)
        self.state = State(word_to_guess=word_to_guess)

    def play(self) -> None:
        while self.state.num_failed_trials < self.num_lives and\
                not self._check_win():
            self._display()
            inp_char = input().lower()
            self._step(inp_char=inp_char)

    def _display(self) -> None:
        print(HANGMAN_PICS[self.state.num_failed_trials])
        print(self._format_ans())
        print("Used chars: " + (" ".join(self.state.used_char)).upper())

    def _format_ans(self) -> str:
        answer = ""
        for c in self.state.word_to_guess:
            if c in self.state.used_char:
                answer += f" {c} "
            else:
                answer += " _ "
        return answer.upper()

    def _step(self, inp_char) -> bool:
        if inp_char in self.state.used_char:
            print(f"You have already entered {inp_char}! Try another one")
            return
        self.state.used_char.add(inp_char)
        if inp_char in self.state.word_to_guess:
            print("Nice!")
            if self._check_win():
                print("You won the game!")
        else:
            self.state.num_failed_trials += 1
            print("This char is not in the word you are trying to guess!")
            if self.state.num_failed_trials < self.num_lives:
                print(
                    "Now you have "
                    f"{self.num_lives - self.state.num_failed_trials} lives."
                )
            else:
                print("You loose!")

    def _check_win(self) -> bool:
        for c in self.state.word_to_guess:
            if c not in self.state.used_char:
                return False
        return True
