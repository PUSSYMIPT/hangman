import argparse

from hangman.game import Game


def main(args):
    with open(args.words_file, "r") as inp_file:
        words = tuple(line for line in inp_file)
    play_game: bool = True
    while play_game:
        game = Game(words=words)
        game.play()
        print("Game end. Do you want to play again? (Y/n)")
        ans = input().lower()
        while ans not in ["y", "n"]:
            print("Game end. Do you want to play again? (Y/n)")
            ans = input().lower()
        play_game = ans == "y"


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--words-file",
        type=str,
        default="words.txt",
        help="File with word on every line."
    )

    args = parser.parse_args()
    main(args)
