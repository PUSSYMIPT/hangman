from dataclasses import dataclass, field
from typing import Set


@dataclass
class State:
    word_to_guess: str
    used_char: Set[str] = field(default_factory=set)
    num_failed_trials: int = 0
